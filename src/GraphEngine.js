/**
 * Implement and experiment graph traversal features
 * Wrap edges and vertices into classes to allow chaining in Javascript
 *
 * Classes
 *  Set, abstract class for common methods in Vertex and Edge
 *  Vertex, represents a set of nodes in the graph
 *  Edge, represents a set of links between nodes
 *  GraphEngine, hosts data and allows manipulation
 */

/*
 * Set, graph element
 * Abstract class for the Edge and Vertex
 */
var Set = function(e,g) {
  var items = [];
  if (e instanceof Array)       items = e;
  else if (e instanceof Object) items = [e];
  
  // Private methods
  var copy = function(j) {
    return JSON.parse(JSON.stringify(j));
  }
  var exists = function(i) {
    var res = false;
    items.map(function(_i) {
      if (_i.id==i.id && !_i._track)
        res = true;
    })
    return res;
  }
  
  // Protected method
  this.getItems = function() {
    return items;
  }
  
  // Public methods
  this.count = function() {
    return items.length;
  }
  this.countBy = function(key) {
    var out = {};
    items.map(function(i) {
      if (i[key] in out==false) out[i[key]]=0;
      out[i[key]]++;
    });
    return out;
  }
  this.has = function(attr,value) {
    return this.filter(function(i) {
      return i[attr]==value;
    });
  }
  this.filter = function(f) {
    var filteredItems = items.filter(f);
    switch (this.constructor.name) {
      case "Vertex": return new Vertex(filteredItems,g);
      case "Edge":   return new Edge(filteredItems,g);
    }
  }
  this.values = function(attr) {
    return items.map(function(v) {
      return v[attr];
    });
  }
  var untrack = function(i) { // remove all tracked data
    var ic = copy(i);
    delete ic._track; // delete _track
    for (var k in ic) { // delete all keys containing .
      if (k.indexOf(".")!=-1) delete ic[k];
    }
    return ic;
  }
  this.as = function(ref) {
    items.map(function(i) {
      if (!i._track) i._track = {};
      i._track[ref] = untrack(copy(i));
    });
    return this;
  } 
  this.add = function(i) {
    if (i instanceof Array) {
      i.map(function(i_) {
        if (!exists(i_)) items.push(i_);
      });
    } else {
      if (!exists(i)) items.push(i);
    }
    return this;
  }
  this.data = function(track) {
    return copy(items.map(function(i) {
      if (track) {
        i._track=track;
        for (var ref in track) {
          var keys = track[ref];
          for (var k in keys) {
            i[ref+"."+k] = keys[k];
          }
        }
      }
      return i;
    }));
  }
  return this;
}


/*
 * Vertex, set of vertices
 */
var Vertex = function(v,g) {
  var that = this;
  Set.call(this,v,g);
  
  // Private methods
  var _E = function(label,dir) {
    var e = new Edge(null,g);
    that.getItems().map(function(v) {
      if (label) e.add(g.E().has("label",label).has(dir,v.id).data(v._track));
      else       e.add(g.E().has(dir,v.id).data(v._track));
    });
    return e;
  }
  var _ = function(label,f,dir) {
    var e = f(label).data();
    var v = new Vertex(null,g);
    e.map(function(i) { v.add(g.V(i[dir]).data(i._track)); });
    return v;
  }
  
  // Public methods
  this.inE = function(label) { // inner edges
    return _E(label,"out");
  }
  this.outE = function(label) { // outer edges
    return _E(label,"in");
  }
  this.out = function(label) { // follow edges
    return _(label,this.outE,"out");
  }
  this.in = function(label) { // back follow edges
    return _(label,this.inE,"in");
  }
  return this;
}


/*
 * Edge, set of edges
 */
var Edge = function(e,g) {
  var that = this;
  Set.call(this,e,g);
  
  // Private method
  var _V = function(label,dir) {
    var v = new Vertex(null,g);
    that.getItems().map(function(e) {
      if (!label || e.label==label) {
        v.add(g.V(e[dir]).data(e._track));
      }
    });
    return v;
  }
  
  // Public methods
  this.outV = function(label) { // target vertices
    return _V(label,"out");
  }
  this.inV = function(label) {  // source vertices
    return _V(label,"in");
  }
  return this;
}


/*
 * GraphEngine, hosts data representing the graph
 */
var GraphEngine = function(connector) {
  var that = this;
  var _ = function(type,id) {
    var opts = {
      item: { V: new Vertex(null,that), E: new Edge(null,that) },
      get:  { V: connector.V.get,  E: connector.E.get },
      scan: { V: connector.V.scan, E: connector.E.scan }
    };
    var e = opts.item[type];
    if (id instanceof Array) {
      id.map(function(i) { e.add(opts.get[type](i)); });
      return v;
    } else if (id) {
      var i = opts.get[type](id);
      if (i) e.add(i,that);
      return e;
    } else {
      return e.add(opts.scan[type](),that);
    }
  }
  this.V = function(id) { return _("V",id); }
  this.E = function(id) { return _("E",id); }

  // Data manipulation
  this.delV = function(id) {
    var v = that.V(id);
    var outerE = v.outE().data(), // locate outer edges
        innerE = v.inE().data();  // locate inner edges
    var res = connector.V.delete(id); // delete vertex from db
    outerE.map(function(e) {
      if (that.E(e.id).inV().count()==0) connector.E.delete(e.id);
    });
    innerE.map(function(e) {
      if (that.E(e.id).outV().count()==0) connector.E.delete(e.id);
    });
    return new Vertex(res,that);
  }
  this.delE = function(id) {
    return new Edge(connector.E.delete(id),that);
  }
  this.updateV = function(id,o) {
    return new Vertex(connector.V.update(id,o),that);
  }
  this.updateE = function(id,o) {
    var res = null;
    if ((o.in!=undefined && g.V(o.in).count()==0)==false
     && (o.out!=undefined && g.V(o.out).count()==0)==false) { // prevent adding edges between unknown edges
      res = connector.E.update(id,o);
    }
    return new Edge(res,that);
  }
  this.addV = function(id,o) {
    return new Vertex(connector.V.put(id,o),that);
  }
  this.addE = function(id,o) {
    var res = null;
    if (o.in!=undefined && g.V(o.in).count()!=0
     && o.out!=undefined && g.V(o.out).count()!=0) { // prevent adding edges between unknown edges
      res = connector.E.put(id,o);
    }
    return new Edge(res,that);
  }
  return this;
}
