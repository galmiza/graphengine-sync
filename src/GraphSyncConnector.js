/*
 * Connector to a sync database
 * Public methods
 *  GraphSyncConnector load(Array)
 *  Object             get(id)
 *  Array              scan()
 *  Object             delete(id)
 *  Object             put(id,o)
 *  Object             update(id,o)
 *  Array              query(id)
 */
var GraphSyncConnector = function() {
  var items = [];
  var copy = function(j) {
    return JSON.parse(JSON.stringify(j));
  }
  this.load = function(data) {
    items = copy(data);
    return this;
  }
  this.get = function(id) {
    var filteredItems = items.filter(function(i) { return i.id==id; });
    if (filteredItems.length==1)      return copy(filteredItems.pop());
    else if (filteredItems.length==0) return null;
    else {
      console.log("ERROR multiple items with id",id);
      return null;
    }
  }
  this.scan = function() {
    return copy(items);
  }
  this.delete = function(id) {
    var res = null;
    if (id) {
      for (var i=0;i<items.length;i++) {
        if (items[i].id==id) {
          res = items.splice(i,1).pop();
        }
      }
    } else {
      items.length = 0;
    }
    return res;
  }
  this.put = function(id,o) {
    var res = null;
    var item = items.filter(function(i) { return i.id==id; }).pop();
    if (!item) {
      items.push({id:id});
      res = this.update(id,o);
    }
    return res;
  }
  this.update = function(id,o) {
    var item = items.filter(function(i) { return i.id==id; }).pop();
    if (item) {
      for (var k in o) {
        var value = o[k];
        if (k=='id') continue; // prevent changing id
        item[k] = value;
      }
    }
    return item;
  }
  this.query = function(id,o) {
    // not implemented
    return null;
  }
  return this;
}