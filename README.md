# Introduction

This project is an experimental graph-engine query language implementation in Javascript.  
It runs synchronously and manipulates graph data in the memory.  
It is not optimized, doesn't use any indexing method, thus shall be used for educational purpose only.

The language specification is strongly inspired by [Gremlin](https://tinkerpop.apache.org/gremlin.html), a graph traversal language.

# Definitions

## Vertex

A vertex is a node in the graph.  
It has:

 * a unique identifier, usually 'id'
 * a type, commonly named 'label' (examples: User, Game, Room)
 *  ... and any additional properties

## Edge

An edge is a link in the graph. It connects two vertices.  
It also has:

 * a unique identifier, usually 'id'
 * a directed relationship, commonly named 'label' (examples: likes, owns, operates)
 *  ... and any additional properties

# Language specification

A graph traversal query is a chain of operations.  
Each operation takes the output of the previous one as input.  

The input can be one of the following type:

 * a graph (instance of GraphEngine)
 * a set of vertices (instance of Vertex)
 * a set of edges (instance of Edge)

Depending on the input type, different operations are available to traverse the graph.

### Class GraphEngine

```
Vertex V()           # returns a subset with all the vertices
Vertex V(id)         # returns a subset with the vertex matching the id
Vertex addV(id,o)    # adds a vertex to the graph and returns a subset with the created vertex
Vertex updateV(id,o) # updates a vertex by id and returns a subset with the updated vertex
Vertex delV()        # deletes all vertices and returns a subset with the deleted vertices
Vertex delV(id)      # deletes a vertex by id and returns a subset with the deleted vertex
Edge   E()           # returns a subset with all the edges
Edge   E(id)         # returns a subset with the edge matching the id
Edge   addE(id,o)    # adds an edge to the graph and returns a subset with the created edge
Edge   updateE(id,o) # updates an edge by id and returns a subset with the updated edge
Edge   delE()        # deletes all edges and returns a subset with the deleted edges
Edge   delE(id)      # deletes an edge by id and returns a subset with the deleted edge
```

### Class Vertex

```
Number count()       # returns the number of vertices in the input subset
Object countBy(attr) # returns a kv objects showing occurences of different values of attr in input the subset
Edge   outE(label)   # returns outer edges of given label from vertices in the input subset
Edge   inE(label)    # returns inner edges of given label from vertices in the input subset
Vertex out(label)    # returns vertices along outer edges of given label from vertices in the input subset
Vertex in(label)     # returns vertices along inner edges of given label from vertices in the input subset
Vertex has(attr,value) # returns vertices that match the filter on attribute
Vertex filter(f)     # returns vertices that match the filter given as a function
Array  values(attr)  # returns an array with the values of an attribute
Array  data()        # returns vertices as an Array of Objects (instead of an instance of Vertex)
Vertex as(ref)       # adds ref as a key to the _track attribute of vertices in the input subset
```

### Class Edge

```
Number count()       # returns the number of edges in the input subset
Vertex outV(label)   # returns vertices moving forward along current edges of a given type
Vertex inV(label)    # returns vertices moving backward along current edges of a given type
Edge   has(attr,value) # returns edges that match the filter on attribute
Edge   filter(f)     # returns edges that match the filter given as a function
Array  data()        # returns edges as an Array of Objects (instead of an instance of Edge)
Edge   as(ref)       # adds ref as a key to the _track attribute of edges in the input subset
```

# Getting started

Considering the following graph with 10 vertices (5 of type "A", 5 of type "B"):

![img](../../downloads/graph.png)

```javascript
// Create a graph engine instance
var g = new GraphEngine({
  V: new GraphSyncConnector(),
  E: new GraphSyncConnector()
});

// Run queries
g.V().count(); // returns the number of vertices
/* 10 */

g.V("a3").out().in().data(); // returns vertices after we followed, forward then backward, all edges along vertex "a3"
/* [
 {id:"a3",label:"A"},
 {id:"a5",label:"A"}
] */

g.V().countBy("label"); // returns number of values for label
/* {
  A:5,
  B:5
} */
```

# Zoom on features

## Tracking

Tracking is the ability to trace the traversal of any given set of items (vertices or edges).  
At each traversal operation (out,in,outV,inV,outE,inE) the input items are not propagated to the output.  
Yet it is sometimes useful to know what "path" in the graph an item followed.

```javascript
g.V().has("label","A").as("A").out().outE().data()
/* [
  {"id":"e7 ","in":"b1","label":"shoots","out":"a2","A.id":"a1","A.label":"A"},
  {"id":"e8 ","in":"b1","label":"shoots","out":"a4","A.id":"a1","A.label":"A"},
  {"id":"e9 ","in":"b2","label":"shoots","out":"a1","A.id":"a1","A.label":"A"},
  {"id":"e10","in":"b2","label":"shoots","out":"a2","A.id":"a1","A.label":"A"},
  {"id":"e12","in":"b4","label":"shoots","out":"a4","A.id":"a1","A.label":"A"},
  {"id":"e13","in":"b4","label":"shoots","out":"a5","A.id":"a1","A.label":"A"},
  {"id":"e11","in":"b3","label":"shoots","out":"a5","A.id":"a3","A.label":"A"},
  {"id":"e11","in":"b3","label":"shoots","out":"a5","A.id":"a5","A.label":"A"}
] */
```

In this example (based on the graph shown above),

 * we mark vertices of type "A" with the reference "A" (method 'as'),
 * we then follow the outter edges from these vertices (method 'out') and end up with vertices of type "B",
 * we then get the outter edges from these vertices of type "B" (method 'outE').

As expected, the output edges all go from vertex of type "B" to vertex of type "A".  
The tracking feature tells us, for each output edge, what was the first vertex it comes from.

It is possible to mark items at different steps of the graph traversal as shown below.

```javascript
g.V().has("label","A").as("A").out().as("B").outE().data()
/* [
  {"id": "e7","in":"b1","label":"shoots","out":"a2","A.id":"a1","A.label":"A","B.id":"b1","B.label":"B"},
  {"id": "e8","in":"b1","label":"shoots","out":"a4","A.id":"a1","A.label":"A","B.id":"b1","B.label":"B"},
  {"id": "e9","in":"b2","label":"shoots","out":"a1","A.id":"a1","A.label":"A","B.id":"b2","B.label":"B"},
  {"id":"e10","in":"b2","label":"shoots","out":"a2","A.id":"a1","A.label":"A","B.id":"b2","B.label":"B"},
  {"id":"e12","in":"b4","label":"shoots","out":"a4","A.id":"a1","A.label":"A","B.id":"b4","B.label":"B"},
  {"id":"e13","in":"b4","label":"shoots","out":"a5","A.id":"a1","A.label":"A","B.id":"b4","B.label":"B"},
  {"id":"e11","in":"b3","label":"shoots","out":"a5","A.id":"a3","A.label":"A","B.id":"b3","B.label":"B"},
  {"id":"e11","in":"b3","label":"shoots","out":"a5","A.id":"a5","A.label":"A","B.id":"b3","B.label":"B"}
] */
```

Note that once tracking is enabled, the output items appear as many times as there are paths from any marked item to the item.  
The above output shows two times the edge "e11" as there are two paths from marked vertices "A" to edge "e11":

 * "a3" => "b3" => "e11"
 * "a5" => "b3" => "e11"
